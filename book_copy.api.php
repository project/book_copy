<?php

/**
 * Implements hook_book_copy_goto_alter().
 *
 * Allow for redirection after a book has been cloned
 */
function hook_book_copy_goto_alter($book) {
  drupal_goto('node/' . $book->nid);
}

/**
 * Implements hook_book_copy_alter().
 *
 * Allow for modification of each item during the clone of the book items.
 */
function hook_book_copy_alter($node, $bid, $newbid) {
	// specific operation for book outline 6
  if ($node->type == 'page' && $bid == 6) {
		$node->title .= ' cloned';
	}
}